const validator = require('validator');
const isEmpty = require('./isEmpty/isEmpty');

const validateRegisterInput = (data) =>{
    const error = {};

    // convert no data to empty string
    data.name = !isEmpty(data.name) ? data.name : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    data.password2 = !isEmpty(data.password2) ? data.password2 : '';
    
    if(!validator.isLength(data.name, {min:2, max:50})) error.name = 'Character must be between 2 to 50 characters';
    if(validator.isEmpty(data.name)) error.name = 'Name is empty';
    
    if(!validator.isEmail(data.email)) error.email = 'input must be an email';
    if(validator.isEmpty(data.email)) error.email = 'Email is empty';
    
    if(!validator.isLength(data.password, {min:6})) error.password = 'password must be more than 6 characters';
    if(validator.isEmpty(data.password)) error.password = 'Password is empty';
    
    if(!validator.equals(data.password, data.password2)) error.password2 = 'confirm password must match';
    if(validator.isEmpty(data.password2)) error.password2 = 'Confirm password is empty';
    

    return {
        error,
        isValid: isEmpty(error)
    };
};

module.exports = validateRegisterInput;