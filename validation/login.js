const validator = require('validator');
const isEmpty = require('./isEmpty/isEmpty');

const validateLoginInput = (data) =>{
    const error = {};

    // convert no data to empty string
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
        
    if(!validator.isEmail(data.email)) error.email = 'input must be an email';
    if(validator.isEmpty(data.email)) error.email = 'Email is empty';
    
    if(validator.isEmpty(data.password)) error.password = 'Password is empty';  

    return {
        error,
        isValid: isEmpty(error)
    };
};

module.exports = validateLoginInput;