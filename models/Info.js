const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InfoSchema = new Schema({
    user:{
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
    to:{
        type: [String],
        require: true,
    },
    body:{
        type: String,
        require: true,
    }
});

const Info = mongoose.model('info', InfoSchema);

module.exports = Info;