const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Creating the user instance of the schema
const UserSchema = new Schema({
    name:{
        type: String,
        require: true,
    },
    email:{
        type: String,
        require: true,
    },
    password:{
        type: String,
        require: true,
    },
    date:{
        type: Date,
        default: Date.now(),
    }
});

// Creating a model with the schema
const User = mongoose.model('user',UserSchema);

module.exports = User;