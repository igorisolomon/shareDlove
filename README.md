# **shareDlove**

## Table of contents

* Overview
* Team
* Frameworks
* Features

## Overview

The entrepreneurship journey is full of ups and downs, and a simple word of encouragement can do a lot in motivating the entrepreneur
The shareDlove application is an online app that allows anybody to share a word of love anonymously to a colleagues phone via SMS.

## Team

Jacky Kimani: Frontend developer  
Solomon Igori: Backend developer

## Framework

* React js
* Node js
* Express js
* Kirusa API

## Features

**Register**  
User should be able to register using only Meltwater email

* Name: *any random name*  
* Email: *meltwater email only*  
* Password: *must be more than 6 characters*  
* Confirm password: *must match the password*

**Login**  
User should be able to log in  

* Email: *must be a registered email*  
* Password: *must be the right password*

**Send Message**  
User should be able to send messages to the colleague's phone number  

* Number: *colleague number*  
* Messages: *make your colleague happy*
