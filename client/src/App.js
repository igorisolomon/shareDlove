import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store';

import './App.css';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import Landing from './components/Landing';
import SendMessage from './components/SendMessage';
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Route exact path="/" component={Landing} />
            <Route exact path="/Register" component={Register} />
            <Route exact path="/Login" component={Login} />
            <Route exact path="/SendMessage" component={SendMessage} />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
