import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class SendMessage extends Component {

    constructor() {
        super()
        this.state={
            number: '',
            message:'',
            errors:{}
        }
        this.onChange=this.onChange.bind(this)
        this.onSubmit=this.onSubmit.bind(this)

    }
    onChange(e){
        this.setState({[e.target.number]:e.target.value})
    }
    onSubmit(e){
        e.preventDefault()
    const user={ 
            email: this.state.number,
            password: this.state.message
        }
    console.log(user)
    }

  render() {
    return (
     
      <div className="SendMessage">
    <div className="container">
      <div className="row">
        <div className="col-md-8 m-auto">

        <Link to="/">
           <h1 className="display-3 mb-4 msg">EIT Messenger</h1>
           </Link>
            <p className="msg"> Send Messages to Fellow EIT's</p>
            <hr/>
          {/* <h1 className="display-4 text-center">Log In</h1> */}
          <p className="lead text-center">Send Message</p>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <input type="number" 
              className="form-control form-control-lg" 
              placeholder="Phone Number" 
              name="number"
              value ={this.state.number}
              onChange = {this.onChange} />
            </div>

            <div className="form-group">
              <input type="text" 
              className="form-control form-control-lg" 
              placeholder="Enter Message" 
              name="message"
              value ={this.state.message}
              onChange = {this.onChange} />
            </div>
            <input type="submit" className="btn btn-info btn-block mt-4" />
          </form>
        </div>
      </div>
    </div>
  </div>

    )
  }
}

export default SendMessage
