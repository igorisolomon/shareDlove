import React, { Component } from 'react';
import { Link } from 'react-router-dom';
 
class Landing extends Component {
  hide(){
    document.querySelector('.hide').style.display = 'none';
    document.querySelector('.links').style.marginTop = '100px';
  }
  render() {
    return (
        <div className="landing">
        <div className="dark-overlay landing-inner text-light">
          <div className="container">
            <div className="row">
              <div className="col-md-12 text-center">
                <h1 className="display-3 mb-4 msg">EIT Messenger
                </h1>
                <p className="msg"> Send Messages to Fellow EITs</p>
                <hr />
                <div className="createAccount">
                <h2 className="msg">New Customer</h2>
                <p className="msg">
                    New to EIT mesenger create<br/>a new account and get started today 
                </p>
                <Link to="/register" className="btn btn-lg btn-info mr-2">Create Account</Link>
                </div>
                
                <div className="userLogin">
                <h2 className="msg" >Registered User</h2>
                <p className="msg">Have an account?<br/>Sign in now. </p>
                <Link to="/login" className="btn btn-lg btn-info mr-2">Sign In</Link>
                </div>
              </div>
            </div>
            {/* <div className="row">
              <Link to="/SendMessage" className="btn btn-lg btn-info mr-2 myMsg">Send Message</Link>
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default Landing;
