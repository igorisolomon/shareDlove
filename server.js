const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');

const user = require('./routes/api/user');
const info = require('./routes/api/info');

const app = express();

// using passport middleware
app.use(passport.initialize());

// Get passport from config
require('./config/passport')(passport);

// using body-parser middleware
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// db config
const db = require('./config/keys').mongoURI;
// connecting to mongodb
mongoose
    .connect(db, { useNewUrlParser: true })
    .then(()=>console.log('connected to mongodb'))
    .catch(err=>console.log(err.message));

app.get('/',(req,res)=>res.send('Hello'));
app.use('/api/user', user);
app.use('/api/info', info);

const port = process.env.PORT || 5000;

app.listen(port,()=>console.log(`Server running on http://localhost:${port}`));