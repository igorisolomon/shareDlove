const express = require('express');
const route = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const keys = require('../../config/keys');

// import user model
const User = require('../../models/User');

// importing validation
const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');

/**
 * @route   GET api/user/test
 * @desc    user detail
 * @access  Public
 */
route.get('/test', (req, res) => res.json({ msg: 'user' }));

/*-----------------------------------------------------------------------------------
 * @route POST api/user/register
 * @desc user registration
 * @access Public
 */
route.post('/register', (req, res) => {
    // validate using validator
    const { error, isValid } = validateRegisterInput(req.body);
    // check if it is valid
    if (!isValid) return res.status(400).json(error);

    // check for email
    User.findOne({ email: req.body.email })
        .then(user => {
            error.email = 'email already exist';
            if (user) return res.status(404).json(error);
            else {
                // Create new user
                const newUser = new User({
                    name: req.body.name,
                    email: req.body.email,
                    password: req.body.password
                });
                // encrypt password
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;
                        // save user
                        newUser.save()
                            .then(user => res.json(user))
                            .catch(err => res.json(err));
                    });
                });
            }
        });
});
/*---------------------------------------------------------------------------------
 * @route POST api/user/login
 * @desc login user
 * @access Public
 */
route.post('/login', (req, res) => {
    // get validator
    const { error, isValid } = validateLoginInput(req.body);
    if (!isValid) return res.status(400).json(error);

    // get email and password
    const email = req.body.email;
    const password = req.body.password;

    // check email
    User.findOne({ email })
        .then(user => {
            // if it doesnt exit, alert user
            if (!user){
                error.email = 'User not found';
                return res.status(404).json(error);
            }

            // if it exist, check password
            else {
                bcrypt.compare(password, user.password)
                    .then(isMatch => {
                        // if it does login
                        if (isMatch) {
                            // create payload
                            const payload = {
                                id: user.id,
                                name: user.name,
                            };
                            // sign token
                            jwt.sign(
                                payload,
                                keys.secretOrKey,
                                { expiresIn: 60 * 60 * 3 }, //expires in 3hrs
                                (err, token) => {
                                    res.json({
                                        msg: 'success',
                                        token: 'Bearer ' + token
                                    });
                                });
                        }
                        // if it doesn't match, alert user
                        else {
                            error.password = 'Wrong password';
                            res.status(400).json(error);
                        }
                    });
            }
        });
});
/**--------------------------------------------------------------------------------------------- 
 * @route api/user/current
 * @desc for authentication
 * @access Private
*/
route.get('/current',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        res.json({
            id: req.user.id,
            name: req.user.name,
            email: req.user.email
        });
    }
);

module.exports = route;