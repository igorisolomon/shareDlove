const express = require('express');
const route = express.Router();

/**
 * @route   GET api/info
 * @desc    user detail
 * @access  Private
 */
route.get('/info',(req,res)=>res.json({msg:'info'}));

module.exports = route;